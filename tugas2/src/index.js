import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
//import App from './App';
import * as serviceWorker from './serviceWorker';
//import Login from './component/Login/Login'
//import Home from './component/Home/Home'
import Router from './Router.jsx'
//import NavBar from './component/NavBar/NavBar'
import 'bootstrap/dist/css/bootstrap.min.css';

ReactDOM.render(
    <Router />,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
