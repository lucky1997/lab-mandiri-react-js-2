import React from 'react'
import { Image } from 'react-bootstrap'
import Hitung from '../Hitung/Hitung'

export default function Column(props) {
    return (
        <React.Fragment>
            <td>
                <Image src={props.gambar} style={{width:'100%'}} />
                <b>{props.nama}</b>
                <Hitung />
            </td>
        </React.Fragment>
    )
}
