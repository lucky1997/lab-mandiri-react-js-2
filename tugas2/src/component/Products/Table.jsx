import React from 'react'
import Column from './Column'
import Image1 from '../../asset/image/background.jpg' 
import Image2 from '../../asset/image/smooties.jpg' 

export default function Table() {
    return (
        <table className='table'>
            <tr>
                <Column gambar={Image1} nama="Sepeda Bagong" />
                <Column gambar={Image2} nama="Smooties" />
            </tr>
        </table>
    )
}
