import React, { useEffect, useState } from 'react'
import { Button, Form } from 'react-bootstrap'

export default function Hitung() {
    const[hitung, setHitung] = useState(0)

    useEffect(() => {
        document.title = '${hitung}'
    })
    return (
        <Form inline>
            <Button className="btn btn-success" onClick={()=>setHitung(hitung+1)}>+</Button>
            <Form.Label><b>{hitung}</b></Form.Label>
            <Button className="btn btn-success" onClick={()=>setHitung(hitung-1)}>-</Button>
        </Form>
    )
}
