import React, { Component } from 'react'
import {Redirect} from 'react-router-dom'
import { Form, Button } from 'react-bootstrap'
import './Login.css'
import { login } from '../../utils'

export default class Login extends Component {
    constructor(props){
        super(props)
        this.controlLogin = this.controlLogin.bind(this);
        this.state = {
            user : "",
            pass : "",
            isLogin: false
        }
    }

    controlLogin() {
        const isUser = document.getElementById("username").value
        const isPass = document.getElementById("password").value
        if(isUser === 'abcde' && isPass === '12345')
        {
            login();
            this.props.history.push('/products')
        }
    }
    render() {
        return (
            <div className="Login">
                <div className="auth-inner">
                    <h2 style={{textAlign:'center'}}>Login</h2>
                        <Form onSubmit={this.controlLogin}>
                            <Form.Group controId="formGroupUsername">
                                <Form.Label>Username</Form.Label>
                                <Form.Control type="text" placeholder="Enter Username" id="username"/>
                            </Form.Group>
                            <Form.Group controId="formGroupPassword">
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="password" placeholder="Enter Password" id="password"/>
                            </Form.Group>
                            <Button variant="primary" type="submit" className="btn btn-primary btn-block">Login</Button>
                        </Form>
                    </div>
                </div>
            )
        } 
}
    // render() {
    //     const isLogin = this.state.isLogin
    //     if(isLogin === false)
    //     {
    //         return (
    //             <div className="Login">
    //                 <div className="auth-inner">
    //                     <h2 style={{textAlign:'center'}}>Login</h2>
    //                     <Form onSubmit={this.controlLogin}>
    //                         <Form.Group controId="formGroupUsername">
    //                             <Form.Label>Username</Form.Label>
    //                             <Form.Control type="text" placeholder="Enter Username" id="username"/>
    //                         </Form.Group>
    //                         <Form.Group controId="formGroupPassword">
    //                             <Form.Label>Password</Form.Label>
    //                             <Form.Control type="password" placeholder="Enter Password" id="password"/>
    //                         </Form.Group>
    //                         <Button variant="primary" type="submit" className="btn btn-primary btn-block">Login</Button>
    //                     </Form>
    //                 </div>
    //             </div>
    //         )
    //     } 
    //     else {
    //         return (
    //             <Redirect to="/products" />
    //         );
    //     }
    // }
