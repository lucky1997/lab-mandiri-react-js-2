import React, { Component } from 'react'
import Image1 from '../../asset/image/background.jpg' 
import { Image } from 'react-bootstrap'

export default class Home extends Component {
    render() {
        return (
            <Image src={Image1} style={{width:'100%'}} fluid className='image-holder'/>
        )
    }
}
