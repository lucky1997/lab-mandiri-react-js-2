import React, { Component } from 'react'
import { Navbar, Nav } from 'react-bootstrap'
import { logout, isLogin } from '../../utils'

export default class NavBar extends Component {
    constructor(props) {
        super(props);
        this.state ={
            isLogin: isLogin()
        }
        this.handleLogout = this.handleLogout.bind(this)
    }

    handleLogout() {
        logout();
        this.setState({
            isLogin: false
        })
    }
    render() {
        return (
            <div>
                <Navbar bg="dark" variant="dark">
                    <Navbar.Brand href="/">Litle Store</Navbar.Brand>
                    <Nav className="mr-auto">
                        <Nav.Link href="/">Home</Nav.Link>
                        { this.state.isLogin ?
                            <Nav>
                                <Nav.Link href="/products">Products</Nav.Link>
                                <Nav.Link onClick={this.handleLogout}>LogOut</Nav.Link>
                            </Nav>
                            // <Nav.Link innerRef={this.handleLogout}></Nav.Link>
                            : <Nav.Link href="/login">Login</Nav.Link>
                        }
                    </Nav>
                </Navbar>
            </div>
        )
    }
}
