import React from 'react'
import {BrowserRouter, Switch} from 'react-router-dom'
import NavBar from './component/NavBar/NavBar'
import Home from './component/Home/Home'
import Login from './component/Login/Login'
import Products from './component/Products/Products'
import PublicRoute from './PublicRoute'
import PrivateRoute from './PrivateRoute'


export default function Router() {
    return (
        <BrowserRouter>
        <NavBar/>
            <Switch>
                <PublicRoute restricted={false} exact path="/" component={Home} />
                <PublicRoute restricted={true} exact path="/login" component={Login} />
                <PrivateRoute exact path="/products" component={Products} />
            </Switch>
        </BrowserRouter>
    //     <BrowserRouter>
    //     <NavBar/>
    //     <Route exact path="/" component={Home} />
    //     <Route exact path="/login" component={Login} />
    //     <Route exact path="/products" component={Products} />
    // </BrowserRouter>
    )
}